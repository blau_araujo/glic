# Glossário

## A

## B

## C

## D

## E

## F

## G

## H

## I

## J

## K

## L

### Linux

É um *kernel* (núcleo), um programa componente do sistema operacional cuja função é fornecer uma camada de abstração para que os demais programas tenham acesso aos recursos de processamento e dispositivos do hardware.

## M

## N

## O

## P

## Q

## R

## S

## T

## U

### Usar Linux

Você não **usa o [Linux](#Linux)**, o Linux é um kernel. Os programas do sistema operacional instalados na sua máquina podem usar o Linux, mas você não tem como usar. Se o sistema operacional estiver instalado em um celular, provavemente ele será o **Android**, que utiliza uma modificação kernel Linux. Se o sistema operacional no seu computador veio de uma insatalação de distribuições como Debian, Ubuntu ou Mint, por exemplo, elas são distribuições do sistema operacional GNU com o kernel Linux, também conhecido como o sistema operacional GNU/Linux.



### Usuário

Pessoa que consome sistematica e repetidamente produtos químicos, programas proprietários ou programas como serviços (SaaS). Esses produtos são conhecidos por causar dependência física, psicológica, dormência do senso crítico e outras deficiências cognitivas graves. Ver [utilizador](#Utilizador).

### Utilizador

Quem que faz uso de alguma coisa; quem controla.

## V

## W

## X

## Y

## Z
