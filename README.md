# Glossário de Expressões Livremente Corretas

Pequeno glossário colaborativo de termos e expressões amplamente difundidos entre ~~usuários~~ utilizadores de **[GNU/]** Linux e Software ~~Gratuito~~ Livre, mas que, propositalmente ou não, levam a ideias e conceitos totalmente equivocados.
